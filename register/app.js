var app = angular.module('aguagente-website', [
	'ngFileUpload',
	'ngResource',
	'ui.router',
	'mgo-angular-wizard',
	'angular-loading-bar'
]);

app.config(function($httpProvider) {
	$httpProvider.defaults.withCredentials = true;
	$httpProvider.defaults.timeout = 25000;
});


app.run(function(AuthService){
	// Conekta.setPublishableKey("key_TGFAZrzeFTSi71VbMPfmCkg"); // Produccion
	Conekta.setPublishableKey("key_MaFzT3SvrAEBkonXV3gg2HQ"); // Sandbox
	AuthService.getToken();
});
app.controller('registro', function registro($scope,$filter,$window,$timeout,AuthService,
	SessionService,WizardHandler,Elements,Categories,
	Upload,Payment,Imagen,Contracts) {

	$scope.data = {};
	$scope.contractData = {
        "fecha"                : $filter('date')(new Date(), 'yyyy-MM-dd'),
        "material"             : {},
        "elements"             : {},
        "mensual"              : 0,
        "responsabilidad"      : true,
        "monthly_installments" : 1,
        "fee_installments"     : 0,
        "credit_card"          : {}
    };
    $scope.relationCard = {
        "amex" : "AMERICAN_EXPRESS",
        "visa" : "VISA",
        "mastercard" : "MC"
    };
    $scope.tarjeta = {};
    $scope.vendedor = {};
    $scope.totalCurrentContract = 0;
	$scope.currentUser = SessionService;
	$scope.contractFotos = [];
	$scope.img_brand = null;
	$scope.mostrarTotal = false;

	Payment.getFees().then(function(response){

		$scope.fees = response;
		$scope.fee = $scope.tarjeta.bank ? $scope.fees[$scope.relationCard[$scope.tarjeta.bank]] : null;

    });

    $scope.next = function(){
    	$window.scrollTo(0, 0);
    	WizardHandler.wizard().next();
    };

    $scope.validarTarjeta = function(data){
		if( !Conekta.card.validateNumber(data.card_number) ) return 'Número de tarjeta invalido';
		if( !Conekta.card.validateCVC(data.cvc) ) return 'CVC incorrecto';
		if( !Conekta.card.validateExpirationDate(data.expiration_month,data.expiration_year) ) return 'Fecha de vencimiento invalida';
		if( !data.bank ) return 'Tipo de tarjeta obligatorio';
		if( Conekta.card.getBrand( data.card_number.toString() ) != data.bank ) return 'Tipo de tarjeta incorrecto';
		return false;
	};

	$scope.changeBrand = function(){
		if($scope.tarjeta.bank){

			$scope.fee = $scope.fees[$scope.relationCard[$scope.tarjeta.bank]];
			$scope.contractData.fee_installments = parseFloat($scope.fees[$scope.relationCard[$scope.tarjeta.bank]][$scope.contractData.monthly_installments]);

		}
		else
			$scope.fee = null;
	};

	$scope.checkBrand = function(){
		var brand = $scope.tarjeta.card_number ? Conekta.card.getBrand( $scope.tarjeta.card_number ) : null;
		if(brand) $scope.img_brand = brand+'.png';
		else $scope.img_brand = null;
	};

	$scope.changeFormaPago = function(){

		$scope.contractData.fee_installments = parseFloat($scope.fees[$scope.relationCard[$scope.tarjeta.bank]][$scope.contractData.monthly_installments]);

	};

	$scope.preview = function(){
		$scope.extras = [];
	    $scope.total_extras = 0;
	    $scope.resp_social = 0;
	    $scope.resp_mensual = 0;

	    $scope.extras.push({
	        "price" : $scope.contractData.material.price,
	        "name" : $scope.contractData.material.name
	    });

	    $scope.total_extras += parseFloat($scope.contractData.material.price);

	    angular.forEach( $scope.contractData.elements, function(value, key){

	        $scope.extras.push({
	            "price" : value.price,
	            "name" : value.name
	        });

	        $scope.total_extras += parseFloat(value.price);
	        
	    });

	    if($scope.contractData.responsabilidad){
	        $scope.resp_social = ($scope.total_extras / 1.16) * 0.007;
	        $scope.resp_mensual = ( ($scope.vendedor.sign_into.monthly_fee / 100) / 1.16) * 0.007;
	    }

	    $scope.total_extras += $scope.resp_social;
	    $scope.mostrarTotal = false;
	};


	$scope.enviarDatosPago = function(){

		if(!$scope.tarjeta.card_number){
			$scope.preview();
			$scope.next();
		}

		var error = $scope.validarTarjeta($scope.tarjeta);

		if(error){
			swal("Oops", error, "error");
		}
		else{

			$scope.contractData.credit_card = angular.copy($scope.tarjeta);
			$scope.preview();
			$scope.next();

		}
		return;
	};

    $scope.uploadImage = function(file,index){
    	if(file){
	    	Upload.base64DataUrl(file).then(function(url){
	    		$scope.contractFotos[index] = url;
	    	});
    	}
    };

    $scope.removeImage = function(index){
    	$scope.contractFotos[index] = null;
    };

    $scope.enviarDatos = function(form){
    	if(form.$invalid){
    		swal("Oops", "Campos invalidos", "error");
    		form.$submitted = true;
    		return;
    	}
    	$scope.next();
    };


	$scope.referir = function(){

		var referido = $scope.data.referido ? $scope.data.referido : 'aguagratis@aguagente.com';

		if (!/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/.test( referido ) ) {
			swal("Oops", "Email invalido", "error");
		}

		$scope.data.referido = referido;
		
		AuthService
			.getClientByEmail( referido )
			.then(function(response){

				$scope.data.email = '';

				if(response.success && response.response.length){
					
					SessionService.setGuest(response.response[0]);

					$scope.vendedor = SessionService.getGroupSeller();
					$scope.group = SessionService.getGroupSeller();
    				$scope.deposito = parseFloat($scope.vendedor.sign_into.deposit) / 100;
    				$scope.contractData.mensual = parseFloat( $scope.vendedor.sign_into.monthly_fee ) / 100;
					Categories.getAll().then(function(response){

				        $scope.categories = response;

				        $scope.material = $filter('filter')(response,{'name':'Material'})[0];

				        if($scope.material){
				            var acero = $filter('filter')($scope.material.elements,{'name':'Acero inoxidable'})[0];
				            if(acero){
				                acero.price = parseFloat( $scope.vendedor.sign_into.installation_fee ) / 100;
				            }
				        }

						var element = $scope.material ? $scope.material.elements[0] : {};
				        if(Object.keys($scope.contractData.material).length > 0){

				            if($scope.contractData.material.id_categories_elements)
				        	    var elegido = $filter('filter')($scope.material.elements,{'id_categories_elements': $scope.contractData.material.id_categories_elements});
				            else{
				                var elegido = $filter('filter')($scope.material.elements,{'name': $scope.contractData.material.element_name});

				            }
				        	element = elegido ? elegido[0] : element;

				        }
						$scope.changeMaterial(element);
						$scope.mostrarTotal = true;
						$scope.next();

				    });

				}
				else{

					swal("Oops", "El email que ingresaste no es vendedor", "error");

				}

			},function(response){
				console.log(response);
				swal("Oops", "Algo anda mal", "error");
			});

	};

	$scope.changeMaterial = function(elem){
        $scope.contractData.material = elem;
    };

    $scope.toggleExtra = function(extra){
		if( $scope.contractData.elements[extra.id_categories_elements] ){
			delete $scope.contractData.elements[extra.id_categories_elements];
		}
		else{
			$scope.contractData.elements[extra.id_categories_elements] = extra;
		}
	};

	$scope.toggleResponsabilidad = function(){
		$scope.contractData.responsabilidad = !$scope.contractData.responsabilidad;
	};

	$scope.$watch('contractData', function(newVal,OldVal){
        
        var sum = 0;
        sum += newVal.mensual;
        angular.forEach(newVal.elements, function(value, key) {
            sum += parseFloat(value.price);
        });
        sum += Object.keys(newVal.material).length > 0 ? parseFloat(newVal.material.price) : 0;

        var resp = (sum / 1.16) * 0.007;

        sum += parseFloat( $scope.vendedor.sign_into.deposit ) / 100;
        sum += newVal.responsabilidad ? resp : 0;
        $scope.totalCurrentContract = sum;

    }, true);


    $scope.enviarContrato = function(formData){

		Contracts.send(formData).then(function(data){

			swal("Correcto!", "Contrato enviado exitosamente", "success");
			$timeout(function(){
				window.location = 'index.html';
			},2000);

		}, function(err){

			var email_existe = err.data && err.data.response && err.data.response.errors && err.data.response.errors['client.email'] ? true : false;
			var msj_existe = err.data && err.data.response == 'User is already a client' ? true : false;

			if(email_existe || msj_existe){

				swal({
				  title: "Oops",
				  text: "El email del contrato ya ha sido previamente registrado",
				  type: "input",
				  showCancelButton: true,
				  closeOnConfirm: false,
				  animation: "slide-from-top",
				  inputPlaceholder: "Ingresa un nuevo email"
				},
				function(inputValue){
				  if (inputValue === false) return false;
				  
				  if (inputValue === "") {
				    swal.showInputError("Necesitas ingresar el email nuevo!");
				    return false
				  }
				  
				  $scope.contractData.email = inputValue;
				  $scope.terminar();
				});

			}
			else{

				swal("Oops", "Error al enviar el contrato", "error");

				var error = err.data ? err.data : err;

				var obj = {
	                'payload' : formData,
	                'error' : error
	            };
            	$scope.currentUser.notifyError(JSON.stringify(obj));

			}

		});
	};

	$scope.datosConekta = function(data,name){
		return  {
			"name"      : name,
			"number"    : data.card_number,
			"cvc"       : data.cvc,
			"exp_month" : data.expiration_month,
			"exp_year"  : data.expiration_year
		};
	};

	$scope.terminar = function(){

		var pictures = {
			6 : "proof_of_address",
			7 : "id",
			8 : "id_reverse",
			9 : "profile"
		};

		var formData = new FormData();
		var contract = $scope.contractData;
		
		formData.append('client[name]', contract.name);
		formData.append('client[address]', contract.address);
		formData.append('client[phone]', contract.phone);
		formData.append('client[email]', contract.email);
		formData.append('client[between_streets]', contract.between_streets);
		formData.append('client[colony]', contract.colony);
		formData.append('client[state]', contract.state);
		formData.append('client[county]', contract.county);
		formData.append('client[postal_code]', contract.postal_code);
		formData.append('client[id_groups]', $scope.currentUser.getGroupSeller().id_groups );
		formData.append('responsabilidad', contract.responsabilidad );
		formData.append('seller', SessionService.getIdClient() );
		formData.append('monthly_installments', contract.monthly_installments );
		formData.append('elements[]', contract.material.id_categories_elements );

		angular.forEach(contract.elements, function(value,key){
			formData.append( 'elements[]', key );
		});

		contract.fotos = $scope.contractFotos;

		for(var i=0; i<contract.fotos.length; i++){

			var blob = Imagen.base64ToBlob( contract.fotos[i] );
			if(blob){
				var imageName = 'image'+i+'.'+(blob.mime.replace('image/',''));
				var name = i < 6 ? 'photos[]' : pictures[i];
				formData.append( name, blob.file, imageName );
			}
			
		}

		if($scope.tarjeta && $scope.tarjeta.card_number){
			var datos = $scope.datosConekta($scope.tarjeta, $scope.contractData.name);
			Conekta.token.create({"card":datos}, 
				function(token){
					formData.append('credit_card', token.id );
					$scope.enviarContrato(formData);
				}, 
				function(response){
					swal("Oops", "Error al guardar la tarjeta en conekta, verifica que los datos de la tarjeta sean los correctos", "error");
					$scope.currentUser.notifyError(JSON.stringify(response));
				}
			);
		}
		else{
			$scope.enviarContrato(formData);
		}	

	};

});

app.filter('DiasMes', function (){
	return function (dias) {
        return Math.round( dias / 30.41666 );
    };
});

app.filter("trustAsHtml", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);

app.filter("trustSrc", ['$sce', function($sce) {
  return function(src){
    return $sce.trustAsResourceUrl(src);
  }
}]);

var apiAguagente = {
	"URL": 'http://panel.aguagente.com/api/public',
	//"URL": 'http://localhost:8888/arizaga/aguagente-api/public',
	"FACE" : 'http://tiny.cc/p9f6fy'
	//"FACE": "http://tiny.cc/4923fy",
	//"PROFILE_IMAGES": ''
};

app.constant('API', apiAguagente);