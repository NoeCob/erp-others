function Imagen ($http,$q) {


	return  {

		urlToBase64 : function(imgUrl){

			var defer = $q.defer();
			var img = new Image();
		    img.setAttribute('crossOrigin', 'anonymous');
		    img.onload = function() {
				var canvas = document.createElement("canvas");
				canvas.width = img.width;
				canvas.height = img.height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0);
				var dataURL = canvas.toDataURL("image/jpeg");
				defer.resolve(dataURL);
		    }
		    img.src = imgUrl;
		    return defer.promise;

		},

		base64Image : function(img){

			img.setAttribute('crossOrigin', 'anonymous');
			var canvas = document.createElement("canvas");
			canvas.width = img.width;
			canvas.height = img.height;
			var ctx = canvas.getContext("2d");
			ctx.drawImage(img, 0, 0);
			var dataURL = canvas.toDataURL("image/png");
			return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");

		},

		base64ToBlob : function(dataURI){
			if( !angular.isString(dataURI) ) return null;
			var byteString;
			if(dataURI.split(',')[0].indexOf('base64') >= 0) byteString = atob(dataURI.split(',')[1]);
			else byteString = unescape(dataURI.split(',')[1]);
			var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
			var ia = new Uint8Array(byteString.length);
			for (var j = 0; j < byteString.length; j++) 
				ia[j] = byteString.charCodeAt(j);

			return {
				"file" : new Blob([ia], {type:mimeString}),
				"mime" : mimeString
			};
		},

		blobImage : function(img){
			img.setAttribute('crossOrigin', 'anonymous');
			var canvas = document.createElement("canvas");
			canvas.width = img.width;
			canvas.height = img.height;
			var ctx = canvas.getContext("2d");
			ctx.drawImage(img, 0, 0);
			return canvas.toBlob(function(blob){
				return blob;
			});
		},

		urlToBlob : function(imageUrl){

			return  $http({
				        url: imageUrl,
				        method: "GET",
				        responseType: "blob",
				        withCredentials : false,
				        headers : {
				        	'X-CSRF-TOKEN': undefined
				        }
				    })
					.then(function(response){
						return response.data;
					});
		}

	}

}

Imagen.$inject = ['$http','$q'];

app.service('Imagen',Imagen);