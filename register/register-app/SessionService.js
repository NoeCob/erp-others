var SessionService = function ($window,$state,$q,$http,$filter,API) {

    this.session = {
        'user': null,
        'guest' : null
    };

    this.update = function(){

        var _this = this;
        var user  = _this.getUser();
        var guest = _this.getGuest();
        var defer = $q.defer();

        if(user){
            $http
            .get(
                API.URL + '/users/' + user.id
            )
            .then(function (response){

                var data = response.data;
                var role = $filter('filter')(data.response.roles, {'name':'Client'});
                if( !role.length ){
                    _this.deleteUser();
                    $state.go('login');
                    defer.reject('No tiene permisos de cliente');
                }
                else{
                    $http.get(
                        API.URL + '/clients?id_users='+ user.id
                    )
                    .success(function(client){
                        data.response.client = client.response[0];
                        _this.setUser(data.response);
                    });

                }
                defer.resolve(_this.getUser());

            },function (err) {
                defer.reject('Failed');
            });
        }
        else{
            var guest = _this.getGuest();
            if(guest){
                $http.get(
                    API.URL + '/clients?id_users='+ guest.id_users
                )
                .then(function(client){
                    _this.setGuest(client.data.response[0]);
                    defer.resolve(client.data.response[0]);

                },function(err){

                    defer.resolve(guest);

                });
            }
            else defer.reject('Failed');
        }

        return defer.promise;
    }

    this.getClientStatus = function(){
        var user = this.getUser();
        if( angular.isObject( user ) ) return user.client.status;
        return null;
    }

    this.getClientLevel = function(){
        var user = this.getUser();
        if( angular.isObject( user ) ) return user.client.level;
        return null;
    }

    this.getInfo = function(){
        if( angular.isDefined($window.localStorage['aguagente-website']) ){
            this.session = angular.fromJson($window.localStorage['aguagente-website']);
            return this.session;
        }
        return this.session;
    }

    this.getGroupSeller = function(){
        var user = this.getUser();
        if(user) return user.client.group;
        else{
            var guest = this.getGuest();
            if(guest) return guest.group;
        }
        return null;
    }

    this.getGroup = function(){
        var user = this.getUser();
        if(user) return user.client.group.sign_into;
        else{
            var guest = this.getGuest();
            if(guest) return guest.group.sign_into;
        }
        return null;
    }

    this.setInfo = function (info){
        this.session = info;
        console.log(info);
        $window.localStorage['aguagente-website'] = angular.toJson(info);
    }

    this.setUser = function (user) {
        this.session.user = user;
        $window.localStorage['aguagente-website'] = JSON.stringify({
            'user': user
        });
    }

    this.setGuest = function (guest) {
        this.session.guest = guest;
        $window.localStorage['aguagente-website'] = JSON.stringify({
            'guest': guest
        });
    }

    this.getIdClient = function(){
        var iduser = this.isClient();
        if(iduser) return iduser;
        else{
            var guest = this.getGuest();
            if(guest) return guest.id_clients;
        }
        return null;
    }

    this.isClient = function() {
        var user = this.getUser();
        if(user){
            var role = $filter('filter')(user.roles, {'name':'Client'});
            return role.length ? user.client.id_clients : null;
        }
        return null;
    }


    this.getUser = function () {
        if( this.session && this.session.user ) return this.session.user;
        else if( this.getInfo() !== null ) return this.session.user;
        return null;
    }

    this.getGuest = function () {
        if( angular.isObject(this.session.guest) ) return this.session.guest;
        else if( this.getInfo() !== null ) return this.session.guest;
        return null;
    }

    this.isSeller = function() {
        var user = this.getUser();
        if(user){
            var role = $filter('filter')(user.roles, {'name':'Seller'});
            return role.length ? true : null;
        }
        return null;
    }

    this.getName = function(){
        var user = this.getUser();
        if(user){
            return user.name;
        }
        return null; 
    }

    this.isFirstPayment = function(){
        var user = this.getUser();
        if(user){
            return user.client.contract.paid_at;
        }
        return null; 
    }

    this.destroy = function() {
        delete this.session.user;
        $window.localStorage['aguagente-website'] = angular.toJson({user:null});
    }

    this.notifyError = function(err){
        return $http({
                    url: "http://softmart.mx/error.php",
                    method: "POST",
                    data : "garry="+err,
                    withCredentials : false,
                    headers : {
                        'X-CSRF-TOKEN': undefined,
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                })
                .then(function(response){
                    return response.data;
                });
    }
}

SessionService.$inject = ['$window','$state','$q','$http','$filter','API'];

app.service('SessionService', SessionService);