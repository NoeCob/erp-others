function Categories ($resource, API, $q) {


	var resource = $resource(
		API.URL+'/categories/:id', 
		{
			id: '@id_categories'
		},
		{
			'get': { method:'GET', cache: true}
		}
	);

	return  {
		getAll: function(params) {

			var _this = this;

			return resource
				.get(params)
				.$promise
				.then(function (response) {
					
					return response.response;

				}, function(err){
					console.log('error categorias');
					return [];

				});

		}
	}

}

Categories.$inject = ['$resource', 'API', '$q'];

app.service('Categories',Categories);