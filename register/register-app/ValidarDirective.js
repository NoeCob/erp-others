function validar($compile) {

    return {
        restrict: 'A',
        require:  '^form',
        link: function(scope, el, attrs, formCtrl) {
            var content = angular.element('<p class="validar has-error hidden">'+attrs['validar']+'</p>');
            var elemento = el.parent().hasClass('input-group') ? el.parent() : el;
            content.insertAfter(elemento);

            $compile(content)(scope);
            scope.$watch(function(){
                var invalid = formCtrl[attrs['name']].$invalid;
                var start = formCtrl[attrs['name']].$touched || formCtrl[attrs['name']].$dirty || formCtrl.$submitted;
                content.toggleClass('hidden', !invalid || !start);
                elemento.parent().toggleClass('has-error', invalid && start );
            });
        }
    }
    
}

validar.$inject = ['$compile'];
app.directive('validar', validar);