var AuthService = function ($http, $q, $filter, $timeout, SessionService, API) {

    var authService = {};
    var pushToken = null;


    authService.getClientByEmail = function(email){
        
        var defer = $q.defer();
        $http
            .get( API.URL + '/clients?email='+email )
            .then(function(response){
                defer.resolve(response.data);
            },function(err){
                defer.reject(err);
            });
        return defer.promise;

    }

    authService.unregistered = function(payload){
        var defer = $q.defer();
        $http
            .post(
                API.URL + '/auth/unregistered', 
                payload
            )
            .then(function(response){
                defer.resolve(response);
            },function(response){
                defer.reject(response);
            });
        return defer.promise;
    }

    authService.getToken = function () {

        var _this = this;

        return $http
        .get(API.URL + '/auth/token')
        .then(function (response) { 

            $http.defaults.headers.common['X-CSRF-TOKEN'] = response.data.token;

        }, function(response) {

            $timeout(function(){
                _this.getToken();
            }, 5000);

        });

    }


    authService.logout = function () {

        var defer = $q.defer();
        $http
        .get(API.URL + '/auth/logout')
        .then(function (response) { 

            var res = response.data;
            SessionService.destroy();
            defer.resolve(res.user);

        },function (response) {

            SessionService.destroy();
            defer.reject(response);

        });
        return defer.promise;

    };


    return authService;
    
}

AuthService.$inject = ['$http', '$q','$filter', '$timeout', 'SessionService', 'API'];

app.service('AuthService', AuthService);